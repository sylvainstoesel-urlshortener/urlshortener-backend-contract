![Creative Commons BY-NC-SA license](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).      
&nbsp;
# Sylvain Stoesel's URL Shortener - OpenAPI Contract for the Backend (API)

<img src="https://www.openapis.org/wp-content/uploads/sites/3/2018/02/OpenAPI_Logo_Pantone-1.png" alt="OpenAPI" width="320">

This repository contains the [OpenAPI specification](https://www.openapis.org/) of [Sylvain Stoesel's URL Shortener](https://gitlab.com/sylvainstoesel-urlshortener) project.

The [openapi-contract.yaml](openapi-contract.yaml) file is used in both parts of the project, for code generation:
 - in the [Backend](https://gitlab.com/sylvainstoesel-urlshortener/urlshortener-backend), as the API specification
 - in the [Frontend](https://gitlab.com/sylvainstoesel-urlshortener/urlshortener-frontend), as the API consumer
 
 This repository is used as a [Git Submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules) in both parts of the project.
